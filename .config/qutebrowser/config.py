config.load_autoconfig(False)

config.bind(',m', 'hint links spawn -d mpv {hint-url}')
config.bind(',cm', 'spawn -d mpv {url}')
config.bind(',d', 'hint links spawn -d youtube-dl -P /home/lambda/dls {hint-url}')
config.bind(',od', 'open {url:domain}')

c.tabs.favicons.show = "never"
c.url.searchengines['gh'] = 'https://github.com/search?q={}'
c.url.searchengines['yt'] = 'https://youtube.com/results?search_query={}'
c.url.searchengines['aw'] = 'https://wiki.archlinux.org/index.php?search={}'

c.completion.shrink = True

c.downloads.location.directory = "/home/lambda/dls"

c.editor.command = ['alacritty', '-e', 'vim', '{}']
config.source("tomorrow.py")
